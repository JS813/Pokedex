# Pokedex
 
This application is a basic design of a Pokedex written in Python. You can search pokemon, moves, abilities, and types.

## Installation
To use:
1. download the zip file
1. extract the files
1. run the application (PokedexSearch.exe) file

## How to Use
Begin by starting the application (may take 5-10 seconds to start up, may be longer depending on the speed of your machine)\
A window will pop up in the upper left corner of your primary screen (if you have a multi-screen setup). In the window will be four buttons labeled `Pokedex`, `Type`, `Abilities`, and `Quit`.\
Where ever available, use the `Back` button to return to the previous screen visited and the `Home` button to return to the initial four buttons.\
For some lists of Pokemon, you will need to use the `Previous` and `Next` buttons, located at the top of the window, to scroll through the entire list.
### Pokedex
`Pokedex` will allow you to search by `Regions`, `Location`, `Name`, `Description`, `Body Style`, `Egg Groups`, `Moves`, and `Groups`.
#### Regions
`Regions` will allow you to search for Pokemon according to its region of discovery.
#### Location
`Location` will allow you to search for Pokemon according to where the can be found in a particular region.\
When a region is selected, a list will be shown containing every location in that region.\
If applicable, the corresponding gym badge is shown next to the location it is earned.\
If applicable, the corresponding logo for an extension will be shown next to the locations in the extension.\
When a location is selected, a list of Pokemon will be shown that can be found in that location. If no Pokemon can be found in a particular location, that button will be disabled and the cursor will change to show that it is *not an option*.\
Hover over a Pokemon to see, at the top of the window, in which games that Pokemon can be found at that location.\
You will also see some buttons located at the top of the window that, if applicable, allow to see what Pokemon can be found at different times of day and/or under different weather conditions.\
You can also find a `Reset` button in the upper left that will reset any weather/condition selections made.
#### Name
`Name` will allow you search for Pokemon by the `first letter of their name`, `Mega` evolutions, different `Forms` e.g., gender, regional, etc., region specific forms: `Alolan`, `Galarian`, and `Hisuian`, and `Gigantamax` forms.
#### Description
`Description` will allow you to search for Pokemon by species e.g., The Mouse Pokemon, by the first letter of the first word, then the first word, and then, if neccessary, the second word.
#### Body Style
`Body Style` will allow you to search for Pokemon based on their body style:
* head ![](pics/head.gif)
* head and legs ![](pics/head_legs.gif)
* head and body ![](pics/head_base.gif)
* head and arms ![](pics/head_arms.gif)
* fins ![](pics/fins.gif)
* insectoid ![](pics/insectoid.gif)
* quadruped ![](pics/quadruped.gif)
* single wings ![](pics/single_wings.gif)
* multi wings ![](pics/multiwings.gif)
* multibody ![](pics/multibody.gif)
* tentacles ![](pics/tentacles.gif)
* bipedal-tailed ![](pics/bipedal_tailed.gif)
* bipedal-tailess ![](pics/bipedal_tailess.gif)
* serpenitne ![](pics/serpentine.gif)
#### Egg Groups
`Egg Groups` will allow you to search for Pokemon by breeding groups.
#### Moves
`Moves` will allow you to search for moves by `Type`, `Dynamax` moves, `G-Max` moves, `Z-Moves`, or by their first letter.\
When a move is selected, a list will be shown containing all Pokemon capable of learning this move. In this list, if you hover over a pokemon, you will see, on the left side of the window, what game generation they can learn the move and whether they learn the move:
* by leveling up
* by TM/HM
* when hatched from an egg ![](pics/egg.gif)
* from a move tutor ![](pics/tutor.gif)
#### Groups
`Groups` will allow you to search for Pokemon based on groupings such as *Eeveelution* which will show all involved in the evolution chain of Eevee or the Swords of Justice which will show all Pokemon involved with that group.
### Type
`Type` will allow you to search for Pokemon of a particular type and will also allow you to see type charts for a particular type.\
On the type charts, *hover* over the values to see a description of effectiveness, displayed at the top of the window.\
When looking at a particular type, click the `List`/`Chart` button to switch between the type chart and a list of Pokemon of that type.
### Abilities
`Abilities` will allow you to search for abilities based on the first letter.\
When clicked, the window will show a group of button labeled `A-Z` that will show all abilities that start with the corresponding letter.\
When viewing the ability list, hover over a button for a description of that ability at the top of the window.\
Click on an ability to see a list of which Pokemon are capable of having that ability.
### Quit
`Quit` will end the program and close the window.

## Entry
In every Pokedex entry, you can see the Pokemon's:
* name
* international number
* species name
* type(s)
* height
* weight
* body style
* evolution chain
* `Home` button
* `Back` button
* `Shiny` button
    switches the picture between regular and shiny form
* previous and next buttons
    located at the top of the window to the right, these buttons allow you to scroll through to the Pokemon with international number either directly before or after the currently viewed Pokemon.
* entry description
    for the entry description, a new window will open directly below the primary window with buttons for all of the different games. When a particular generation is selected, you can switch between the games from that generation to see any Pokedex entries that exist for a particular game.

If multiple forms exist for the given selections conditions (may differ depending on where selection is made such as type, moves, or ability), buttons allowing you to switch the picture to another form will appear at the right of the window.

## Licensing
All images and information was acquired from either [Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Main_Page), [Pokemon Database](https://pokemondb.net/pokedex/all), or [serebii.net](https://www.serebii.net/pokemon/).\
This is a personal project and was not built for distribution purposes.
